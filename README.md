# mini projet 4

FER Hugo et RAHMANI Idris GROUPE

===================================================================================================================

exemple de commande pour exécuter le programme : python app.py -rows=10 -cols=10 -cell_size=25 -afforestation='0.6'

===================================================================================================================

Par défaut:
-le nombre de lignes est égal à 15
-le nombre de colonnes est égal à 15
-la taille de chaque case est égale à 30
-le taux de boisement est égal à 0.5

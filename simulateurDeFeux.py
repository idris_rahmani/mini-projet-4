import random

"""fonction qui crée le tableau"""
def createTab(nb_row, nb_col, proba):
    tab = []
    for _ in range(0, nb_row):
        tab2 = []
        tab.append(tab2)
        for _ in range(0, nb_col):
            if(random.random() <= proba):
                tab2.append(1)
            else:
                tab2.append(0)
    return tab

"""fonction qui définit le nombre de voisin en feu"""
def fireNeightborsNumber(tab, row, col):
    nb_neightbors = 0
    if((row != len(tab)-1) and (tab[row+1][col] == 2)):
        nb_neightbors += 1
    if((row != 0) and (tab[row-1][col] == 2)):
        nb_neightbors += 1
    if((col != len(tab[row])-1) and (tab[row][col+1] == 2)):
        nb_neightbors += 1
    if((col != 0) and (tab[row][col-1] == 2)):
        nb_neightbors += 1
    return nb_neightbors

"""fonction qui met à jour l'etat des cases"""
def majBoxSquare(box_value):
    if(box_value == 3):
        box_value = 0
    elif(box_value != 0):
        box_value += 1
    return box_value

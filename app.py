import argparse
import parser
import random
import time
from tkinter import *
from tkinter.ttk import *
from simulateurDeFeux import *

"""Permet d'initialiser la carte"""
def map_initialise():
    map = []
    for row in range(0, args.rows):
        map.append([])
        for column in range(0, args.cols):
            if(tab[row][column] == 0):
                color = 'black'
            elif(tab[row][column] == 1):
                color = 'green'
            map[row].append(canvas.create_rectangle(
                column*args.cell_size, row*args.cell_size,
                column*args.cell_size+args.cell_size,
                row*args.cell_size+args.cell_size, fill=color))
    return map

"""permet de mettre le feu"""
def mettreLeFeu(event):
    x = int(event.x / args.cell_size)
    y = int(event.y / args.cell_size)
    if(tab[y][x] == 1):
        tab[y][x] = 2
        color = 'red'
        canvas.itemconfig(map[y][x], fill=color)
        a_faire.append({'row': y, 'col': x})

"""Permet de placer les arbres"""
def planterArbre(event):
    x = int(event.x / args.cell_size)
    y = int(event.y / args.cell_size)
    if(tab[y][x] == 2):
        tab[y][x] = 1
        color = 'green'
        canvas.itemconfig(map[y][x], fill=color)

"""Fonction qui contient la logique du programme"""
def logique_simulation_feu_de_foret():
    while(a_faire != []):
        boucle_a_faire = a_faire.copy()
        for element in boucle_a_faire:
            if((element['row'] != args.rows-1) and (tab[element['row']+1][element['col']] == 1)):
                a_faire.append(
                    {'row': element['row']+1, 'col': element['col']})
                tab[element['row']+1][element['col']] = 2
            if((element['row'] != 0) and (tab[element['row']-1][element['col']] == 1)):
                a_faire.append(
                    {'row': element['row']-1, 'col': element['col']})
                tab[element['row']-1][element['col']] = 2
            if((element['col'] != args.cols-1) and (tab[element['row']][element['col']+1] == 1)):
                a_faire.append(
                    {'row': element['row'], 'col': element['col']+1})
                tab[element['row']][element['col']+1] = 2
            if((element['col'] != 0) and (tab[element['row']][element['col']-1] == 1)):
                a_faire.append(
                    {'row': element['row'], 'col': element['col']-1})
                tab[element['row']][element['col']-1] = 2
            if(tab[element['row']][element['col']] == 2):
                canvas.itemconfig(map[element['row']]
                                  [element['col']], fill='red')
            if(tab[element['row']][element['col']] == 3):
                canvas.itemconfig(map[element['row']]
                                  [element['col']], fill='grey')
            if(tab[element['row']][element['col']] == 0):
                canvas.itemconfig(map[element['row']]
                                  [element['col']], fill='black')
            if(tab[element['row']][element['col']] == 0):
                a_faire.remove(element)
            tab[element['row']][element['col']] = majBoxSquare(
                tab[element['row']][element['col']])
        master.update()
        time.sleep(2)


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Simulateur de feu de forêt')
    parser.add_argument(
        '-rows', help='nombre de lignes', type=int, default=15)
    parser.add_argument(
        '-cols', help='nombre de colonnes.', type=int, default=15)
    parser.add_argument(
        '-cell_size', help='taille de chaque case.', type=int, default=30)
    parser.add_argument(
        '-afforestation', help='taux de boisement.', type=float, default=0.5)
    args = parser.parse_args()
    if(args.rows <= 0):
        parser.error('Il faut au moins une ligne !')
    if(args.cols <= 0):
        parser.error('Il faut au moins une colonne !')
    if(args.cell_size <= 0):
        parser.error('La taille des cases doit être au moins de 1 !')
    if(args.afforestation <= 0 and args.afforestation >= 1):
        parser.error('Le taux de boisement doit être compris entre 0 et 1 et ne peut pas être nul !')

    tab = createTab(args.rows, args.cols, args.afforestation)

    master = Tk()
    master.title("Simulation de Feu de Forêt")

    master.rowconfigure(0, weight=1)
    master.rowconfigure(1, weight=1)
    master.columnconfigure(1, weight=1)

    canvas_width = args.cols*args.cell_size
    canvas_height = args.rows*args.cell_size
    canvas = Canvas(master, width=canvas_width, height=canvas_height)
    canvas.grid(row=0, padx=10, pady=10, sticky='nsew')
    canvas.pack
    map = map_initialise()

    a_faire = []

    canvas.bind('<Button-1>', mettreLeFeu)
    canvas.bind('<Button-3>', planterArbre)

    button_start = Button(master, text='Start',
                          command=logique_simulation_feu_de_foret)
    button_start.grid(row=1)

    master.mainloop()

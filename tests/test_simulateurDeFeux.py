import unittest

from simulateurDeFeux import *


class TestSimulateurDeFeux(unittest.TestCase):
    
    def test_fireNeightborsNumber(self):
        tab = []
        for _ in range(0, 10):
            tab2 = []
            tab.append(tab2)
            for _ in range(0, 10):
                tab2.append(0)
        tab[5][5] = 2
        tab[5][4] = 2
        tab[5][6] = 2
        tab[4][5] = 2
        tab[1][1] = 2

        self.assertEqual(fireNeightborsNumber(tab, 5, 5), 3)
        self.assertEqual(fireNeightborsNumber(tab, 1, 1), 0)

    def test_majBoxSquare(self):
        self.assertEqual(majBoxSquare(0), 0)
        self.assertEqual(majBoxSquare(1), 2)
        self.assertEqual(majBoxSquare(3), 0)
